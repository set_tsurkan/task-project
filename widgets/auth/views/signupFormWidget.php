<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header'=>'<h4>Регистрация</h4>',
    'id'=>'signup-modal',
]);
?>

    <p>Please fill out the following fields to login:</p>

<?php $form = ActiveForm::begin([
    'id' => 'signup-form',
    'enableAjaxValidation' => true,
    'action' => ['site/ajax-signup']
]);
echo $form->field($model, 'username')->textInput();
echo $form->field($model, 'email')->textInput();
echo $form->field($model, 'password')->passwordInput();

?>

    <div class="form-group">
        <div class="text-right">
            <?php
            echo Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']);

            echo Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'signup-button']);
            ?>

        </div>
    </div>

<?php
ActiveForm::end();
Modal::end();