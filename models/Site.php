<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 20.11.2019
 * Time: 16:37
 */

namespace app\models;


class Site
{

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company' => 'Company',
            'title' => 'Заголовок',
        ];
    }
}