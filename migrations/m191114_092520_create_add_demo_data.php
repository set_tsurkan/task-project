<?php

use yii\db\Migration;

/**
 * Class m191114_092520_create_add_demo_data
 */
class m191114_092520_create_add_demo_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->batchInsert('{{%role}}',[
            'title'
        ],[
            ['Администратор'],
            ['Модератор'],
            ['Пользователь'],
        ]);

        $this->batchInsert('{{%task_status}}',[
            'title'
        ],[
            ['Создана'],
            ['В работе'],
            ['На проверке'],
            ['На доработке'],
            ['Выполнена'],
            ['Отменена'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_092520_create_add_demo_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_092520_create_add_demo_data cannot be reverted.\n";

        return false;
    }
    */
}
