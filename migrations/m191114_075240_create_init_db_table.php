<?php

use yii\db\Migration;


class m191114_075240_create_init_db_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //===============Основные настройки проэкта====================================
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'company' => $this->string()->notNull()->unique(),
            'title' => $this->string(),
        ]);

        //===============Таблицы задач и отделов=========================================

        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->string()->notNull(),
            'title' => $this->string()->notNull()->unique(),
            'parent_id' => $this->string(),
        ]);
        $this->addForeignKey('fk_department_company_id', '{{%department}}', 'company_id', '{{%settings}}', 'id');
        $this->addForeignKey('fk_department_parent_department_id', '{{%department}}', 'parent_id', '{{%department}}', 'id');


        $this->createTable('{{%task_status}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
        ]);

        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'date_create' => $this->integer()->notNull(),
            'task_status_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'detail' => $this->text(),
            'implementer_id' => $this->integer(),
            'department_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_task_author_id', '{{%task}}', 'author_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_task_task_status_id', '{{%task}}', 'task_status_id', '{{%task_status}}', 'id');
        $this->addForeignKey('fk_task_implementer_id', '{{%task}}', 'implementer_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_task_department_id', '{{%task}}', 'department_id', '{{%department}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
        $this->dropTable('{{%department}}');
        $this->dropTable('{{%task_status}}');
        $this->dropTable('{{%task}}');
    }
}
