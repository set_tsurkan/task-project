<?php

namespace app\modules\admin\controllers;


use app\models\User;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    //public $layout = '@app/modules/admin/views/default/index';
    
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $user = User::findByRole(Yii::$app->user->identity->username);
            if (!isset($user)) {
                return $this->goHome();
            }
        }
        return $this->render('@app/modules/admin/views/layouts/index');
    }

}
