<?php

namespace app\modules\admin\models;

use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * TaskSearch represents the model behind the search form about `app\modules\admin\models\Task`.
 */
class TaskSearch extends Task
{
    public $datetime_range;
    public $datetime_min;
    public $datetime_max;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'datetime_range',
                'dateStartAttribute' => 'datetime_min',
                'dateEndAttribute' => 'datetime_max',
            ]
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'date_create', 'task_status_id', 'author_id', 'implementer_id'], 'integer'],
            [['title', 'detail'], 'safe'],
            [['datetime_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['task_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['task_status_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		// Решаю проблему чувствительности символов к регистру в операторе LIKE для Sqlite.
		// Fix case sensitive in like operator in Sqlite. This line does not affect other types of databases.
		\alhimik1986\yii2_crud_module\web\ModelHelper::sqliteFixCaseSensitiveInLikeOperator(static::getDb());

		$query = Task::find();
        $query->joinWith(['department' => function ($q) {}]);
        $query->joinWith(['task_status' => function ($q) {}]);
        $query->joinWith(['user' => function ($q) {}]);
		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,


			// Пейджер и число записей на страницу
			// Sets the pager and results per page
			'pagination' => array('pageSize' => (isset($params['per-page']) AND (int)$params['per-page']) ? abs((int)$params['per-page']) : 10),
		]);

		$this->load($params);

		if (!$this->validate()) {
			$query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$t = self::tableName();
		$query->andFilterWhere([
             $t.'.id' => $this->id,
             $t.'.task_status_id' => $this->task_status_id,
             $t.'.author_id' => $this->author_id,
             $t.'.implementer_id' => $this->implementer_id,
             $t.'.department_id' => $this->department_id,
            'department.title'       => $this->department,
            'task_status.title'       => $this->task_status,
            'user.username'       => $this->user,
        ]);

        $query->andFilterWhere(['like', $t.'.title', $this->title])
            ->andFilterWhere(['like', $t.'.detail', $this->detail]);

        $query->andFilterWhere(['>=', $t.'.date_create', $this->datetime_min])
            ->andFilterWhere(['<', $t.'.date_create', $this->datetime_max]);

		// Сортировка по нескольким полям таблицы, и вывод результата в виде массива, а не в виде объектов (для экономии памяти)
		// Sorting by several fields of the table and return result as array, not as object (to prevent memory leaks).
		$query->orderBy(\alhimik1986\yii2_crud_module\web\Sort::init()->setOrderByDefault('')->getOrder($params, $this));
		$query->asArray();

		return $dataProvider;
	}



    }


