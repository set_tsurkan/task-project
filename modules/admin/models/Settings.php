<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $company
 * @property string $title
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company'], 'required'],
            [['company', 'title'], 'string', 'max' => 255],
            [['company'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Company',
            'title' => 'Заголовок',
        ];
    }


    public static function getTree()
    {
        $data = self::find()->all();
        $tree[] = 'Без родителя';

        foreach ($data as $item) {
            $tree[$item['id']] = $item['company'];
        }
        return $tree;
    }
}
