<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task_status".
 *
 * @property int $id
 * @property string $title
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    public static function getTree($exclude = 0, $root = false)
    {
        $data = self::find()->all();
        $tree[] = '';

        if ($root) {
            $tree = [];
        }
        foreach ($data as $item) {
            $tree[$item['id']] = $item['title'];
        }
        return $tree;
    }

    public static function getList()
    {
        $list = self::find()->all();
        return ArrayHelper::map($list, 'id', 'title');
    }
}
