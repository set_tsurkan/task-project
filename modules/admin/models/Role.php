<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $title
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }



    public static function getTree()
    {
        $data = self::find()->all();
        $tree[] = '*******';

        foreach ($data as $item) {
            $tree[$item['id']] = $item['title'];
        }
        return $tree;
    }
}
