<?php

namespace app\modules\admin\models;

use app\models\User;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $title
 * @property integer $date_create
 * @property int $task_status_id
 * @property int $author_id
 * @property string $detail
 * @property int $implementer_id
 * @property int $department_id
 * @property Department $department
 * @property Statuses $task_status
 * @property User $user
 *
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'task_status_id', 'author_id', 'department_id'], 'required'],
            [['task_status_id', 'author_id', 'implementer_id', 'department_id'], 'integer'],
            [['detail'], 'string'],
            ['date_create', 'default', 'value' => time()],
            [['title'], 'string', 'max' => 255],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['task_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['task_status_id' => 'id']],
            [['implementer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['implementer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'date_create' => 'Дата создания',
            'task_status_id' => 'Статус',
            'author_id' => 'Инициатор',
            'detail' => 'Подробное описание',
            'implementer_id' => 'Исполнитель',
            'department_id' => 'Отдел',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask_status()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'task_status_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'implementer_id']);
    }
}
