<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $title
 * @property string $parent_id
 * @property string $company_id
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }


    public static function getAllDepartments($parent = 0, $level = 0, $exclude = 0) {
        $children = self::find()
            ->where(['parent_id' => $parent])
            ->asArray()
            ->all();
        $result = [];
        foreach ($children as $department) {
            // при выборе родителя категории нельзя допустить
            // чтобы она размещалась внутри самой себя
            if ($department['id'] == $exclude) {
                continue;
            }
            if ($level) {
                $department['title'] = str_repeat('— ', $level) . $department['title'];
            }
            $result[] = $department;
            $result = array_merge(
                $result,
                self::getAllDepartments($department['id'], $level+1, $exclude)
            );
        }
        return $result;
    }

    public static function getTree($exclude = 0, $root = false)
    {
        $data = self::getAllDepartments(0, 0, $exclude);
        $tree[] = '';

        if ($root) {
            $tree = [];
        }
        foreach ($data as $item) {
            $tree[$item['id']] = $item['title'];
        }
        return $tree;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'parent_id','company_id'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'parent_id' => 'Отдел',
            'company_id' => 'Company',
        ];
    }

//    public function getList($id)
//    {
//        $list = self::find()->where('parent_id', $id)->all();
//        return ArrayHelper::map($list, 'id', 'title');
//    }

    public static function getName($id){
        $department = static::findOne(['id' => $id]);
        return $department->title;
    }


}
