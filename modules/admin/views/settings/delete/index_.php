<?php

use app\modules\admin\models\Settings;
use kartik\form\ActiveForm;
use yii\helpers\Html;

$lastSetting = Settings::find()->max('id');
$model = Settings::find()->where(['id' =>$lastSetting])->one();
if (!isset($model)) {
    $model = new Settings();
}
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="settings-form" type="PO">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?php
                $model->company = $form->field($model, 'company');
                $model->company = $form->field($model, 'title');
                var_dump($model);
            ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
