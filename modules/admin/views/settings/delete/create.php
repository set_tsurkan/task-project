<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Settings */

?>
<div class="settings-create">
    <?= $this->render('index', [
        'model' => $model,
    ]) ?>
</div>
