<?php

use app\modules\admin\models\Settings;
use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$lastSetting = Settings::find()->max( 'id' );
$model = Settings::find()->where( ['id' => $lastSetting] )->one();
if (!isset( $model )) {
    $model = new Settings();
}
?>

<?php
$form = ActiveForm::begin( [
    'action' => ['settings/ajax-setting'],
    'options' => [
        'class' => 'setting-form'
    ]
] );
?>
<?= $form->field( $model, 'company' )->textInput( ['maxlength' => true] ) ?>

<?= $form->field( $model, 'title' )->textInput( ['maxlength' => true] ) ?>

<?= Html::submitButton( "Записать", ['class' => "btn"] ); ?>

<?php ActiveForm::end(); ?>

<script>
    jQuery(document).ready(function ($) {
        $(".setting-form").submit(function (event) {
            event.preventDefault(); // stopping submitting
            var data = $(this).serializeArray();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: data
            })
                .done(function (response) {
                    if (response.data.success == true) {
                        alert("Wow you commented");
                    }
                })
                .fail(function () {
                    console.log("error");
                });

        });
    });
</script>