<?php


use app\modules\admin\models\Settings;




/* @var $form yii\widgets\ActiveForm */

if (Yii::$app->user->isGuest) {
    $visibleContent = false;
} else {
    $visibleContent = true;
}

/* @var $this yii\web\View */
/* @var $setting app\modules\admin\models\Settings */
$lastSetting = Settings::find()->max( 'id' );
$setting = Settings::find()->where( ['id' => $lastSetting] )->one();
$this->title = $setting->company;


?>


<div class="site-index">
<?php if ($visibleContent == true) : ?>

    <?= $this->render(
        'task/table.php'
    )
    ?>


<?php endif; ?>
</div>

