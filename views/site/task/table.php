<?php

use app\models\User;
use app\modules\admin\models\Department;
use app\modules\admin\models\Statuses;
use app\modules\admin\models\TaskSearch;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\widgets\Pjax;

$res = $this->registerJsFile( '@web/js/taskView.js', ['depends' => JqueryAsset::className()] );
$taskId = 0;
?>

    <head>
        <script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link data-require="bootstrap-css@*" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    </head>


<div class="body-content">

    <?php Pjax::begin(); ?>
    <?php
    $searchModel = new TaskSearch();
    $dataProvider = $searchModel->search( Yii::$app->request->get(), [] );
    $dateRange = DateRangePicker::widget( [
        'model' => $searchModel,
        'attribute' => 'datetime_range',
        'convertFormat' => true,
        'startAttribute' => 'datetime_min',
        'endAttribute' => 'datetime_max',
        'pluginOptions' => [
            'timePicker' => true,
            'timePickerIncrement' => 30,
            'locale' => [
                'format' => 'Y-m-d'
            ]
        ]
    ] );

    ?>

    <?= GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'date_create',
                //'label'=>'Создано',
                'format' => ['date', 'Y-m-d'],
                'filter' => $dateRange,
                'options' => [
                    'style' => 'width: 10%'
                ],
            ],
            [
                'format' => 'text',
                'filter' => Department::find()->select( ['title'] )->indexBy( 'id' )->column(),
                'attribute' => 'department_id',
                'value' => 'department.title',
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'attribute' => 'implementer_id',
                'format' => 'text',
                'filter' => User::find()->select( ['username'] )->indexBy( 'id' )->column(),
                'value' => 'user.username',
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'attribute' => 'title',
                'value' => 'title',
                'format' => 'text',
                'options' => [
                    'style' => 'width: 30%'
                ],
            ],
            [
                'attribute' => 'task_status_id',
                'format' => 'text',
                'value' => 'task_status.title',
                'filter' => Statuses::find()->select( ['title'] )->indexBy( 'id' )->column(),
                'options' => [
                    'style' => 'width: 5%'
                ],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view}',
                'contentOptions' => ['style' => 'width: 8.7%'],
                'visible' => Yii::$app->user->isGuest ? false : true,
                'buttons' => [

                    'view' => function ($url, $model, $key) {
                        $t = 'task/view?id=' . $key;
                        $btn = Html::button( "<span class='glyphicon glyphicon-eye-open'></span>", [
                            'value' => Url::to( $t ), //<---- here is where you define the action that handles the ajax request
                            'class' => 'view-modal-click grid-action btn-xs btn btn-default',
                            'data-toggle' => 'tooltip',
                            'data-target'=>'#view-modal',
                            'data-placement' => 'bottom',
                            'id' => $key,
                            'title' => 'View'
                        ] );
                        return $btn;
                    },
                    'update' => function ($url, $model, $key) {
                        $t = 'task/update?id=' . $key;
                        $btn = Html::button( "<span class='glyphicon glyphicon-pencil'></span>", [
                            'value' => Url::to( $t ), //<---- here is where you define the action that handles the ajax request
                            'class' => 'update-modal-click grid-action btn-xs btn btn-default',
                            'data-toggle' => 'tooltip',
                            'data-target'=>'#update-modal',
                            'data-placement' => 'bottom',
                            'title' => 'Edit'
                        ] );
                        return $btn;
                    },
                ],
            ],


        ],
    ] ); ?>
    <?php Pjax::end(); ?>
</div>



<?php
Modal::begin( [
    'header' => '<h4>View</h4>',
    'id' => 'view-modal',
    'size' => 'modal-lg',
] );

echo $this->render(
    'view.php', ['taskId' => $taskId]);
Modal::end();
?>

<?php
Modal::begin( [
    'header' => '<h4>Update</h4>',
    'id' => 'update-modal',
    'size' => 'modal-lg'
] );
echo $this->render(
    'update.php' , ['id' => $taskId]
);
Modal::end();
?>